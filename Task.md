# Jenkinsfile-CI
---

## Goals
- Configure a CI pipeline that does the following:
  - Get the sources from Bitbucket
  - Build the docker image (created in the previous task)
  - Push the image to the quay.io repository (created in the previous task)

## Considerations
- This Jenkinsfile uses the Dockerfile from the other task to build the service's images.
- You must push the image to the repository created for this service in the Dockerfile's task. (https://quay.io/organization/bootcampseladevops)
- The CI should be triggered automatically after each change on the master branch

## Merging
- To merge this branch into master you must do it via Pull Request
- The Pull Request must be reviewed and approved from another member of the team.
